// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '390',
    width: '640',
    videoId: 'M7lc1UVf-VE',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING && !done) {
    setTimeout(stopVideo, 6000);
    done = true;
  }
}
function stopVideo() {
  player.stopVideo();
}

var $content = $('.menu-content');

function showContent(type) {
  // this assumes that you really must select
  // the content using a class and not an ID (which you've
  // referenced in the href)
  $content.hide().filter('.' + type).show();
}

$('.menu').on('click', '.menu-btn', function(e) {
  // get the type to pass to showContent by grabbing
  // the hash from the anchor href and removing the first
  // character (the #)
  showContent(e.currentTarget.hash.slice(1));
  e.preventDefault();
});

// show 'about' content only on page load (if you want)
showContent('about');
